namespace Blog_t6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accounts", "username", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Password", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "Password", c => c.String(nullable: false));
            DropColumn("dbo.Accounts", "username");
        }
    }
}
