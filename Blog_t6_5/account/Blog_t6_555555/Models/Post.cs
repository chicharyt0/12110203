﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_t6_555555.Models
{
    public class Post
    {
        public int ID { set; get; }
        public String Title { set; get; }
        public String Body { set; get; }
        public DateTime DataCreated { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
    }
}