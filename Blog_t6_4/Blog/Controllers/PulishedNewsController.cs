﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog.Models;

namespace Blog.Controllers
{
    public class PulishedNewsController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /PulishedNews/

        public ActionResult Index()
        {
            var publishednews = db.PublishedNews.Include(p => p.UserProfile);
            return View(publishednews.ToList());
        }

        //
        // GET: /PulishedNews/Details/5

        public ActionResult Details(int id = 0)
        {
            PublishedNews publishednews = db.PublishedNews.Find(id);
            if (publishednews == null)
            {
                return HttpNotFound();
            }
            return View(publishednews);
        }

        //
        // GET: /PulishedNews/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /PulishedNews/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PublishedNews publishednews)
        {
            if (ModelState.IsValid)
            {
                db.PublishedNews.Add(publishednews);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", publishednews.UserProfileUserId);
            return View(publishednews);
        }

        //
        // GET: /PulishedNews/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PublishedNews publishednews = db.PublishedNews.Find(id);
            if (publishednews == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", publishednews.UserProfileUserId);
            return View(publishednews);
        }

        //
        // POST: /PulishedNews/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PublishedNews publishednews)
        {
            if (ModelState.IsValid)
            {
                db.Entry(publishednews).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", publishednews.UserProfileUserId);
            return View(publishednews);
        }

        //
        // GET: /PulishedNews/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PublishedNews publishednews = db.PublishedNews.Find(id);
            if (publishednews == null)
            {
                return HttpNotFound();
            }
            return View(publishednews);
        }

        //
        // POST: /PulishedNews/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PublishedNews publishednews = db.PublishedNews.Find(id);
            db.PublishedNews.Remove(publishednews);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}