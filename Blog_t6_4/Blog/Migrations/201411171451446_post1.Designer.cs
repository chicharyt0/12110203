// <auto-generated />
namespace Blog.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class post1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(post1));
        
        string IMigrationMetadata.Id
        {
            get { return "201411171451446_post1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
