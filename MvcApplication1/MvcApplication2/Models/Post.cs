﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication2.Models
{
    public class Post
    {
        public int ID { set; get; }
        public String TiTle { set; get; }
        public String BoDy { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DataCreated { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }

    }
}