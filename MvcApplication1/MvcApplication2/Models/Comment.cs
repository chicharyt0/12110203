﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public String BoDy { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DataCreated { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now-DataCreated).Minutes;
            }
        }

        public int PostID { set; get; }
        public virtual Post Posts { set; get; }

    }
}