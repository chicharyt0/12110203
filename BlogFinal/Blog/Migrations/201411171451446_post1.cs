namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class post1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "Title", c => c.String());
            DropColumn("dbo.Posts", "Titel");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Posts", "Titel", c => c.String());
            DropColumn("dbo.Posts", "Title");
        }
    }
}
