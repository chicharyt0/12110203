﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "Số kí tự trong khoảng 1 đến 100", MinimumLength = 1)]
        public String Title { set; get; }
        [StringLength(3000, ErrorMessage = "Số kí tự trong khoảng 1 đến 3000", MinimumLength = 1)]
        public String Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}