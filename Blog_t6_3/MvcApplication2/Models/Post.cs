﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication2.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "Số kí trong khoảng 5->500 kí tự!", MinimumLength = 5)]
        public string Title { set; get; }
        [StringLength(2000, ErrorMessage = "Tối thiểu 10 kí tự!", MinimumLength = 10)]
        public string Body { set; get; }
        public DateTime DateCreate { set; get; }
        [DataType(DataType.DateTime, ErrorMessage = "Nhập ngày giờ!")]
        public DateTime DateUpdate { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public int AccountID { set; get; }
        public virtual Account account { set; get; }


        public ICollection<Comment> TiTle { get; set; }

        public DateTime DataCreated { get; set; }
    }
}