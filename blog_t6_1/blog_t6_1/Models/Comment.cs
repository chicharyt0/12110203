﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace blog_t6_1.Models
{
    public class Comment
    {
        [Key]
        public int Id { set; get; }
        public String Body { set; get; }
        public int Post_id { set; get; }
    }
}