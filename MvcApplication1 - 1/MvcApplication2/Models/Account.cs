﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication2.Models
{
    public class Account
    {
        public int ID { set; get; }
        [Required]
        [DataType(DataType.Password, ErrorMessage = "Nhập password!")]
        public String username { set; get; }
        public string Password { set; get; }
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Nhập đúng địa chỉ mail")]
        public string Email { set; get; }
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Tối đa 100 kí tự!")]
        public string FirstName { set; get; }
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Tối đa 100 kí tự!")]
        public string LastName { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}