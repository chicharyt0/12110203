﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required]
        [StringLength(1000, MinimumLength = 10, ErrorMessage = "Tối thiểu 10 kí tự!")]
        public string Body { set; get; }
        public DateTime DateCreate { set; get; }
        public int lastTime
        {
            get
            {
                return (DateTime.Now - DateCreate).Minutes;
            }
        }
        public DateTime DateUpdate { set; get; }
        public string Author { set; get; }
        public int PostID { set; get; }
        public virtual Post post { set; get; }


        public DateTime DataCreated { get; set; }

        public Post Posts { get; set; }
    }
}